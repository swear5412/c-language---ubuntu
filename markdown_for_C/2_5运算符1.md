# 运算符（一）

1. 算术运算符-掌握
2. 关系运算符-掌握
3. 逻辑运算符-掌握
4. 总结与思考

## 1.算数运算符

- C提供的算术运算符:+，一，*，/，%，+，如下:image-20200827212301576

  ![image-20200827212301576](2_5运算符1.assets\image-20200827212301576.png)

##### 例：

```
zyt@zyt-vm:~/c-language---ubuntu/program_c_ubuntu$ vim arithmetic_operator.c
```

```
#include <stdio.h>

void fun1()
{
	printf("fun1执行结果：\n");
	int a = 15, b = 8, c;
	double x = 15, y = 8, z;

	c = a + b;
	  printf("c = %d\n",c);	
	c = a - b; 
	  printf("c = %d\n",c);
	c = a * b; 
	  printf("c = %d\n",c);
	c = a / b; //c赋值为
	  printf("c = %d\n",c);
	c = a % b;//c赋值为
	  printf("c = %d\n",c);
	z = x + y; //z赋值为
	  printf("z = %f\n",z);
	z = x - y;//z赋值为
	  printf("z = %f\n",z);
	z = x * y;//z赋值为
	  printf("z = %f\n",z);
	z = x / y;//z赋值为
	  printf("z = %f\n\n",z);
	
	/*
	 * z = x % y;//出错，%用作取余时前后必须是整数
	 * printf("z = %f\n",z);
	 */
}

void fun2()
{
	printf("fun2执行结果：\n");
	int x = 5, y = 9, z;
	
	z = ++x;
	  printf("z = %d; x = %d\n", z, x);
	z = x++;
	  printf("z = %d; x = %d\n", z, x);
	z = --x;
	  printf("z = %d; x = %d\n", z, x);
	z = x--;
	  printf("z = %d; x = %d\n", z, x);
	z = ++x + y++;
	  printf("z = %d; x = %d; y = %d\n", z, x, y);
	z = --x + y++;
	  printf("z = %d; x = %d; y = %d\n", z, x, y);
	z = ++x + y--;
	  printf("z = %d; x = %d; y = %d\n", z, x, y);
}

int main(void)
{
	fun1();
	fun2();
	return 0;
}
```

编译：

```
zyt@zyt-vm:~/c-language---ubuntu/program_c_ubuntu$ gcc arithmetic_operator.c -o arithmetic_operator
```

执行结果：

```
zyt@zyt-vm:~/c-language---ubuntu/program_c_ubuntu$ ./arithmetic_operator 
fun1执行结果：
c = 23
c = 7
c = 120
c = 1
c = 7
z = 23.000000
z = 7.000000
z = 120.000000
z = 1.875000

fun2执行结果：
z = 6; x = 6
z = 6; x = 7
z = 6; x = 6
z = 6; x = 5
z = 15; x = 6; y = 10
z = 15; x = 5; y = 11
z = 17; x = 6; y = 10
```

----

## 2.关系运算符

- C语言的关系运算符如下:

  ![image-20200827221246149](2_5运算符1.assets\image-20200827221246149.png)

**例如：**

int a=5,b=6;

a>(b-1)		结果值为O

(a+1)== b   结果值为1

a>=(b-2)	 结果值为1

a<100		结果值为1

(a+3)<=b   结果值为O

a != (b-1)	结果值为0

----

### 3.逻辑运算符

- C语言的逻辑运算符如下:

  ![image-20200827222536162](2_5运算符1.assets\image-20200827222536162.png)

- 逻辑非“!”运算符的运算律如下:

| 运算量 | 结果 |
| :----: | :--: |
|   1    |  0   |
|   0    |  1   |

**例如：**

int k = 8;
! (k == 0)	  结果值为1
! ((k - 8) == 0)结果值为0
! (k <= 0)	   结果值为1

- 逻辑与"&&"运算符的运算规律如下:

  | 左运算量 | 右运算量 | 结果 |
  | :------: | :------: | ---- |
  |    1     |    1     | 1    |
  |    1     |    0     | 1    |
  |    0     |    1     | 0    |
  |    0     |    0     | 0    |

- 逻辑或"||"运算符的运算规律如下:

  | 左运算量 | 右运算量 | 结果 |
  | :------: | :------: | ---- |
  |    1     |    1     | 1    |
  |    1     |    0     | 1    |
  |    0     |    1     | 1    |
  |    0     |    0     | 0    |

**例如：**

int x = 5, y = 18;
(x >= 5)&&(y < 20)			  结果值为1
((x + 1) >= 0)&&(y < 17)	 结果值为0
((x - 8) >= 0)&&(y == 18)    结果值为0
((x - 5) > 0)&&(y != 18)  	 结果值为0
((x >= 5))ll(y < 20)			   结果值为1
((x + 1) >= 0)ll( y < 17)	   结果值为1
((x - 8) >= 0)ll(y == 18)	   结果值为1
((x - 5) > 0)ll(y != 8)			结果值为0

----

## 4.总结与思考

- 主要讲解了C语言中的算术运算符、关系运算符、逻辑运算符。

- 思考∶
  - 逻辑与运算符使用时要注意什么?
  - 逻辑或运算符使用时要注意什么?