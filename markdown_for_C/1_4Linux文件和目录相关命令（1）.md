# Linux文件和目录相关命令（1）

##### 终端仿真器

##### 快捷键ctrl+alt+t

右键锁定或收藏至启动器

## 1.Linux的文件系统结构

- 微软Windows操作系统将硬盘上的几个分区，用A:、B:、C:、D:等符号标识。
  存取文件时一定要清楚存放在哪个磁盘的哪个目录下。
- Linux的文件组织模式犹如一颗倒置的树，这与Windows文件系统有很大差别。所
  有存储设备作为这颗树的一个子目录。存取文件时只需确定目录就可以了，无需考
  虑物理存储位置。

（1）Windows文件结构：

逻辑上分区为C D E F G

（2）Linux文件结构

树形的分层组织结构

![image-20200821203427744](1_4Linux文件和目录相关命令（1）.assets/image-20200821203427744.png)文件系统层次结构标准FHS

基本目录

- /bin : bin是二进制(binary）英文缩写。
- /boot:存放的都是系统启动时要用到的程序。
- /dev:包含了所有Linux系统中使用的外部设备。
- /etc:存放了系统管理时要用到的各种配置文件和子目录。
- /lib:存放系统动态连接共享库的。
- /home:普通用户的主目录
- /root:根用户（超级用户）的主目录

## 2.相关指令

#### 1.pwd命令

pwd命令用于显示用户在文件系统中的当前位置，该命令没有任何选项和参数，命执行结果显示为绝对路径名。

```
zyt@zyt-virtual-machine:~$ pwd
/home/zyt
```

### 2.ls列目录内容

ls

ls -a

ls -l  （显示详细信息）

```
zyt@zyt-virtual-machine:~$ ls -l
总用量 44
drwxr-xr-x 2 zyt zyt 4096 8月  12 16:31 公共的
drwxr-xr-x 2 zyt zyt 4096 8月  12 16:31 模板
drwxr-xr-x 2 zyt zyt 4096 8月  12 16:31 视频
drwxr-xr-x 2 zyt zyt 4096 8月  12 16:31 图片
drwxr-xr-x 3 zyt zyt 4096 8月  17 22:45 文档
drwxr-xr-x 2 zyt zyt 4096 8月  18 14:35 下载
drwxr-xr-x 2 zyt zyt 4096 8月  12 16:31 音乐
drwxr-xr-x 2 zyt zyt 4096 8月  12 16:31 桌面
drwxrwxr-x 3 zyt zyt 4096 8月  18 14:37 app
drwxrwxr-x 3 zyt zyt 4096 8月  21 20:19 git_workstation
drwxr-xr-x 3 zyt zyt 4096 8月  12 16:34 snap
```

ls -R

##### 文件类型

![image-20200821205030479](1_4Linux文件和目录相关命令（1）.assets/image-20200821205030479.png)

##### 文件的权限

​		为了对文件进行保护，Linux系统提供了文件存取控制方式。把所有用户划分为3种身份，依次是文件主( user )、同组用户( group )、其他用户( other)。每种用户对一个文件可拥有读(r)、写( w）和执行（x）的权利。

### 3.cd改变目录

```
//绝对路径或相对路径
cd /home/joshua/work
cd project/docs
//回到上一级目录
cd ..
//回到家目录
cd
//回到上一次的工作目录
cd -
```

ctrl+l  清屏

