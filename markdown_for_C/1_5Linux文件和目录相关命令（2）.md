# Linux文件和目录相关命令（2）

## 1.查看文件内容

cat
n加行号

```
zyt@zyt-virtual-machine:~/code$ cat -n 1.txt
     1	123456789abc 
```

head

​	-n文件名(前n行,默认10)
tail
​	-n文件名(后n行,默认10)

## 2.cp复制

```
cp [options] file destination
zyt@zyt-virtual-machine:~/code$ cp 1.txt /home/zyt
```

常用选项

i覆盖时交互提示

r对文件夹递归（文件夹内有文件夹，内部文件夹又存在文件时）

复制多个文件到文件夹cp [options] file1 file2 dest

## 3.mv移动

Usage:

mv [options] file destination

```
mv 2.txt ../
```

移动多个文件:

mv [options] file1 file2 destination

更改文件名

```
mv 1.txt 2.txt
```

## 4.创建和删除文件

touch -创建或更新时间戳

```
zyt@zyt-virtual-machine:~/code$ touch 3.txt
```

rm -删除文件

```
zyt@zyt-virtual-machine:~/code$ rm 3.txt
```

Usage:
-rm [options] <file>...
Example:
rm -i file
-rm -r directory

## 5.创建和删除目录

mkdir -p

```
zyt@zyt-virtual-machine:~$ mkdir code
```

rm -r

```
zyt@zyt-virtual-machine:~$ rm -r code/
```



