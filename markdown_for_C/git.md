# Git实战

## 1.基本概念：

工作目录（working directory）：放代码的目录，在此目录下编辑代码。
代码库（repository）：保存代码的版本数据库。
提交（commit）：将工作目录中的代码保存到代码库。
检出（checkout）：将代码从代码库恢复到工作目录中。
暂存区（staging area）：相当于购物车，因为一次提交的内容必须是完整的，涉及一个问题或特性修改的所有文件，可以将修改的文件先保存到暂存区，然后统一提交到代码库。

## 2.全局配置用户名和email

```
git config --global user.name "朱玉涛"
git config --global user.email “swear5412@163.com”
```

## 3.建立本地代码库与保存代码

```
mkdir smart_speaker  # 创建工作目录
cd smart_speaker  # 进入工作目录
git init  # 创建本地代码库
```

使用git add 文件名/目录名将文件修改保存到暂存区
使用git commit将暂存区的修改提交到代码库

## 4.基本操作代码

查看提交历史记录
git log
比较版本之间的差异
git diff 提交ID
将工作目录中的文件恢复为代码库中的最新版本
git checkout HEAD 
重命名工作目录中的文件
git mv 原文件名 新文件名  
git commit -m "提交日志"
删除工作目录中的文件
git rm 文件名  
git commit -m "提交日志"

## （4）远程代码库

注册并登录码云账号：https://gitee.com/swear5412/
进入码云的代码库主页，点击`克隆/下载`按钮，复制远程代码库地址。
使用`git clone`命令下载代码库到本地，会在当前目录下创建同名工作目录和本地代码库。
同步本地代码库到远程（在本地工作目录中执行）
git push
同步远程代码库到本地
git pull

## （5）提交PR（Pull Request）

Git代码库一般使用Fork-PR流程实现团队开发。
1.点击代码库库主页上的Fork按钮，复制远程代码库到自己的账号。
2.用户修改副本代码库中的内容
3.创建PR（Pull Request）