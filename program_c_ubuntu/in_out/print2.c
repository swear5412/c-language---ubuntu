#include <stdio.h>

//m.n附加格式说明符
int main(int argc, char *argv[])
{
        int a = 1234;
        float f = 123.456;
        char ch = 'a';

        printf("%8d\n%2d\n", a, a);
        printf("%f-%8f-%8.1f-%.2f\n", f, f, f, f);
        printf("ch=%3c\n", ch);

        return 0;
}
