#include <stdio.h>
#include <math.h>
//解一元二次方程
int main(void)
{
	while(1)
	{
	double a, b, c, delta, x1, x2;
	printf("请输入一元二次方程的项a b c:\n");
	scanf("%lf", &a);
	scanf("%*c%lf", &b);
	scanf("%*c%lf", &c);
	delta = b*b - 4*a*c;
	if((a!=0)&&(delta>=0))
	{
		x1 = (-b + sqrt(delta))/(2*a);
		x2 = (-b - sqrt(delta))/(2*a);
		printf("该一元二次方程的解是：\nx1=%.2lf\nx2=%.2lf\n", x1, x2);
	}
	else
	{
		printf("请输入正确的项a b c!\n");
	}
	}
}
