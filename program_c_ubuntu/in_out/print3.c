#include <stdio.h>

int main(int argc, char *argv[])
{
        char s[] = "Hello,world!";

        printf("%s\n", s);
        printf("%15s\n", s);
        printf("%10.5s\n", s);
        printf("%2.5s\n", s);
        printf("%.3s\n", s);
        return 0;
}
