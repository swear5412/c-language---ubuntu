#include <stdio.h>

int main(int argc, char *argv[])
{
        int a = 254;
        float f1 = 345.7;
        char ch = 'a';

        printf("%08d\n%0+8d\n", a, a);
        printf("%09f-%09.2f-%.3f\n", f1, f1, f1);
        printf("ch=%5c\n", ch);

        return 0;
}
