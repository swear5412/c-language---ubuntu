#include <stdio.h>
#include <math.h>
/*
 *（1）如果a为0且b为0，则输出 “Not an equation”（N大写，单词间一个空格）。
（2）如果a为0，退化一次方程，则只输出一个根的值既可以。
（3）如果a不为0，则按以下格式输出方程的根x1和x2（x1和x2之间有一个空格）：
* 若x1和x2为实根，则以x1>=x2输出。
* 若方程是共轭复根，则x1=m+ni，x2=m-ni，其中n>0。
其中x1、x2、m、n均保留2位小数。
*/

int main()
{
	double a,b,c,delta,x1,x2,m,n,i,j;
	scanf("%lf%lf%lf",&a,&b,&c);

	if (fabs(a) <= 1e-6){
		if (fabs(b) <= 1e-6)
			puts("Not an equation");
		else
			printf("%.2lf",-c/b);
	}
	else{
		delta=b*b - 4*a*c;
		m = -b / (2*a);
		n = sqrt(fabs(delta)) / (2*fabs(a));
		i = m + n;
		j = m - n;

		if (delta < 0)
			printf("%.2lf+%.2lfi %.2lf-%.2lfi",m,n,m,n);
		else {
 		       if (i == j)
 			    printf("%.2lf %.2lf",i,i);
  		       else {
 		               x1 = (i > j) ? i : j;
 			       x2 = (i > j) ? j : i;
 			       printf("%.2lf %.2lf", x1, x2);
  			    }
 	    }
			return 0;
	}
}
