#include <stdio.h>

void fun()
{
	static int i = 0;//静态存储类型static，数据类型为整型int的变量i，初值为0
//	int i = 0;//默认存储类型auto
	i = i + 1;
	printf("i = %d\n", i);//打印出i的值
}

int main(void)
{
	fun();
	fun();
	fun();//多次调用fun函数

	return 0;
}
