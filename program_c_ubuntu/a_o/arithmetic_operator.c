#include <stdio.h>

void fun1()
{
	printf("fun1执行结果：\n");
	int a = 15, b = 8, c;
	double x = 15, y = 8, z;

	c = a + b;
	  printf("c = %d\n",c);	
	c = a - b; 
	  printf("c = %d\n",c);
	c = a * b; 
	  printf("c = %d\n",c);
	c = a / b; //c赋值为
	  printf("c = %d\n",c);
	c = a % b;//c赋值为
	  printf("c = %d\n",c);
	z = x + y; //z赋值为
	  printf("z = %f\n",z);
	z = x - y;//z赋值为
	  printf("z = %f\n",z);
	z = x * y;//z赋值为
	  printf("z = %f\n",z);
	z = x / y;//z赋值为
	  printf("z = %f\n\n",z);
	
	/*
	 * z = x % y;//出错，%用作取余时前后必须是整数
	 * printf("z = %f\n",z);
	 */
}

void fun2()
{
	printf("fun2执行结果：\n");
	int x = 5, y = 9, z;
	
	z = ++x;
	  printf("z = %d; x = %d\n", z, x);
	z = x++;
	  printf("z = %d; x = %d\n", z, x);
	z = --x;
	  printf("z = %d; x = %d\n", z, x);
	z = x--;
	  printf("z = %d; x = %d\n", z, x);
	z = ++x + y++;
	  printf("z = %d; x = %d; y = %d\n", z, x, y);
	z = --x + y++;
	  printf("z = %d; x = %d; y = %d\n", z, x, y);
	z = ++x + y--;
	  printf("z = %d; x = %d; y = %d\n", z, x, y);
}

int main(void)
{
	fun1();
	fun2();
	return 0;
}
